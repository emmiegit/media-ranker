#!/usr/bin/python3
from server import Server

if __name__ == "__main__":
    server = Server('/tmp/mediaserver.sock')
    server.run()

