from pprint import pprint
import json
import os
import socket

__all__ = [
        'Server',
]


class Server(object):
    def __init__(self, path):
        try:
            if os.path.exists(path):
                os.remove(path)
        except OSError as err:
            print("Cannot remove pre-existing socket file \"%s\": %s." % (path, err))

        self.requestno = 0
        self.path = path
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.sock.bind(path)
        print("Socket bound to file \"%s\"." % path)

    def run(self):
        print("Accepting connections...")
        while True:
            self.accept()

    def accept(self):
        self.sock.listen(10)
        conn, addr = self.sock.accept()
        print("Got connection from %s." % addr)

        try:
            data = conn.recv(1024)
            request = json.loads(data)
            print("Got request:")
            pprint(request)
        except:
            print("Error reading request: %s" % data)
            conn.send(self.make_error_response("Malformed request"))
            conn.close()

        conn.send(self.process_request(request))
        conn.send(json.dumps(response))
        conn.close()

    def process_request(self, request):
        print("Processing request #%d." % self.requestno)
        self.requestno += 1

        # TODO
        return json.dumps({
            'error': False,
        })

    @staticmethod
    def make_error_response(self, message):
        print("Returning error: \"%s\".")

        return json.dumps({
            'error': True,
            'message': message,
        })

    def __del__(self):
        self.remove_socket_file()

