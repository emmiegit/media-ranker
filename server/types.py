import pickle

__all__ = [
        'RankingData',
        'Show',
        'User',
        'Result',
]

class RankingData(object):
    def __init__(self):
        self.users = {}
        self.media_types = set()
        self.shows = set()

    def save(self, filename):
        pass

    @staticmethod
    def load(self, filename):
        pass


class User(object):
    def __init__(self, username):
        self.name = username
        self.ratings = {}

    def set_rating(self, show, rating):
        if 0 > rating or rating > 10:
            return Result("Rating out of range: %s" % rating)
        elif abs(rating % 0.5) > 0.0001:
            return Result("Rating not divisble by 0.5: %s" % rating)

        self.ratings[show] = rating
        return Result.SUCCESS

    def delete_rating(self, show):
        try:
            del self.ratings[show]
            return Result.SUCCESS
        except KeyError:
            return Result("No such show: %s" % show)

    def __hash__(self):
        return hash(self.name)

    def __str__(self):
        return self.name


class Show(object):
    def __init__(self, name, description, media_type, genres):
        self.name = name
        self.description = description
        self.media_type = media_type
        self.genres = genres

    def __hash__(self):
        return hash((self.name, self.media_type))

    def __str__(self):
        return "%s (%s)" % (self.name, self.media_type)


def Result(object):
    SUCCESS = Result()

    def __init__(self, message=None):
        self.message = message

    def __nonzero__(self):
        return not self.message

