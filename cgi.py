#!/usr/bin/python3
import cgi
import cgitb
import html
import json
import socket

SERVER_HOST = 'media.ammonsmith.me'
SOCKET_PATH = '/tmp/mediaserver.sock'
META_DATA = """
<head>
    <meta charset="UTF-8" />
    <meta name="description" content="Media Review" />

    <title>Media Review</title>
</head>
"""


def print_error(message):
    print("<h3>Error</h3><br>" + message)
    exit()


class Client(object):
    def __init__(self, path):
        try:
            self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            self.sock.connect(path)
        except:
            print_error("Unable to establish connection to the server.")

    def make_request(self):
        try:
            self.sock.send(json.dumps(request))
        except:
            print_error("Unable to send request.")

        try:
            response = json.loads(self.sock.recv(1024))
        except:
            print_error("Server response was malformed.")

        if response['error']:
            print_error(response['message'])

        # TODO process response

    def close(self):
        try:
            self.sock.close()
            self.sock = None
        except:
            print_error("Unable to close socket.")

    def __del__(self):
        if self.sock:
            self.close()


if __name__ == "__main__":
    print("Content-type: text/html\n")
    print(META_DATA)

    if ENABLE_CGI_DEBUG: cgitb.enable()
    form = cgi.FieldStorage()

    if os.environ['SERVER_NAME'] != SERVER_NAME:
        print("This CGI script does not operate on this host.")
        exit()

    print("Under construction.")
    exit()

    client = Client(SOCKET_PATH)
    client.close()

